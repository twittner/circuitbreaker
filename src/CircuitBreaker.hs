-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at http://mozilla.org/MPL/2.0/.

{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE DeriveDataTypeable  #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module CircuitBreaker
    ( CircuitBreaker
    , Failures       (..)
    , CircuitOpen    (..)
    , new
    , run
    , exec
    , getFailures
    ) where

import Control.Applicative
import Control.AutoUpdate
import Control.Monad.Catch
import Control.Monad.IO.Class
import Data.IORef
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Typeable
import Data.Word
import Prelude

type Context m = (Functor m, MonadIO m, MonadCatch m)

data Failures = Failures
    { failureCount :: !Word64
    , failureTime  :: !POSIXTime
    } deriving (Eq, Ord, Show)

zero :: Failures
zero = Failures 0 0

-- | A circuit breaker is configured to execute a monadic action which can
-- fail up to some limit, after which execution will return an error
-- without invoking the action. After some given timeout, the action will
-- be retried and - if successful - resets the failure counter.
data CircuitBreaker = CircuitBreaker
    { threshold   :: !Word64
    , pause       :: !NominalDiffTime
    , handlers    :: !(forall a m . Context m => [Handler m (Either SomeException a)])
    , failures    :: !(IORef Failures)
    , currentTime :: !(IO POSIXTime)
    }

-- | Error type to be returned from 'run' while in open state.
data CircuitOpen = CircuitOpen deriving (Show, Typeable)

instance Exception CircuitOpen

-- | Create a 'CircuitBreaker' which operates on the given set of exceptions.
-- It executes actions up to the given failure limit, after which for the given
-- number of seconds a 'CircuitOpen' error will be returned instead.
new :: MonadIO m => [Handler IO ()] -> Word64 -> NominalDiffTime -> m CircuitBreaker
new hdl trsh delta = liftIO $ do
    f <- newIORef zero
    a <- mkAutoUpdate defaultUpdateSettings { updateAction = getPOSIXTime }
    return $ CircuitBreaker trsh delta (map (mapHandler f a) hdl) f a
  where
    mapHandler f a (Handler handler) = Handler $ \x -> liftIO $ do
        _   <- handler x
        now <- a
        atomicModifyIORef' f $ \k ->
            (k { failureCount = failureCount k + 1, failureTime  = now }, ())
        return (Left $ toException x)

-- | Run the given action and in case of error, return the cause as
-- exception. Only exceptions catched by the set of handlers given to 'new'
-- are considered. Additionally @Left@ can contain 'CircuitOpen' if the
-- circuit has been interrupted.
run :: (Functor m, MonadIO m, MonadCatch m) => CircuitBreaker -> m a -> m (Either SomeException a)
run cb ma = do
    f <- liftIO $ readIORef (failures cb)
    let k = failureCount f
    if k < threshold cb
        then action k `catches` handlers cb
        else do
            now <- liftIO $ currentTime cb
            if failureTime f + pause cb < now
                then action k `catches` handlers cb
                else return $ Left (toException CircuitOpen)
  where
    action 0 = Right <$> ma
    action _ = do
        x <- ma
        liftIO $ atomicWriteIORef (failures cb) zero
        return (Right x)

-- | Like 'run' but throws the @Left@ value as an exception.
exec :: (Functor m, MonadIO m, MonadCatch m) => CircuitBreaker -> m a -> m a
exec cb ma = either throwM return =<< run cb ma

-- | Inspect the current failure count.
getFailures :: MonadIO m => CircuitBreaker -> m Failures
getFailures cb = liftIO $ readIORef (failures cb)
